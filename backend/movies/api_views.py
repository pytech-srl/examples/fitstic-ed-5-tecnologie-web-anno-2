from rest_framework import generics, viewsets

from movies.models import Movie
from movies.serializers import MovieSerializer

__all__ = [
    "MovieViewSet"
]


class MovieViewSet(viewsets.ModelViewSet):
    queryset = Movie.objects.all()
    serializer_class = MovieSerializer
