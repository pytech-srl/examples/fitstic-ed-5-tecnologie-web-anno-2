from django.urls import path

from movies import views

app_name = "movies"

urlpatterns = [
    path("tutti/", views.MovieListView.as_view(), name="movie_list"),
    path("<int:pk>/", views.MovieDetailView.as_view(), name="movie_detail"),
]
