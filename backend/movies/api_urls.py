from rest_framework.routers import SimpleRouter

from movies import api_views

app_name = "api_movies"

router = SimpleRouter()
router.register("movie", api_views.MovieViewSet, basename="movie")

urlpatterns = router.urls
