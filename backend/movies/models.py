from django.db import models


class Movie(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=2048)

    def __str__(self):
        return f"Film {self.id}: {self.title}"
