from django.views.generic import DetailView, ListView

from movies.models import Movie

__all__ = [
    "MovieDetailView",
    "MovieListView",
]


class MovieListView(ListView):
    model = Movie


class MovieDetailView(DetailView):
    model = Movie
