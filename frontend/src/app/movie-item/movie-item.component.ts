import { Component, Input, Output, EventEmitter } from '@angular/core';

import {Movie} from "../models/Movie";

@Component({
 selector: 'app-movie-item',
 templateUrl: './movie-item.component.html',
 styleUrls: ['./movie-item.component.scss'],
})
export class MovieItemComponent {
 @Input() movie!: Movie;
 @Output() deleteMovieEvent: EventEmitter<Movie> = new EventEmitter();

 delete() {
     this.deleteMovieEvent.emit(this.movie);
 }
}
