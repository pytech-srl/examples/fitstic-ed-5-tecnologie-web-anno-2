import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AuthLoginComponent} from './auth/auth-login/auth-login.component';
import {AuthGuard} from './auth/auth.guard';
import { MoviesComponent } from './movies/movies.component';

const routes: Routes = [
    {path: '', component: MoviesComponent, canActivate: [AuthGuard]},
    {path: 'login', component: AuthLoginComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
