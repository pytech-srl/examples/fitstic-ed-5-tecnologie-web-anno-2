import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  readonly API_URL = 'http://localhost:8000/api/v1/auth/';

  constructor(private http: HttpClient) {}

  login(username: string, password: string) {
    return this.http.post<any>(`${this.API_URL}login/`, {
      username: username,
      password: password,
    });
  }

  logout() {
    return this.http.post(`${this.API_URL}logout/`, {});
  }
}
