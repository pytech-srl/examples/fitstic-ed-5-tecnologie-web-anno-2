import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss']
})
export class AuthLoginComponent {
  returnUrl!: string;
  username!: string;
  password!: string;

  constructor(
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  cleanForm() {
    this.username = '';
    this.password = '';
  }

  loginSubmitForm() {
    this.authService.login(this.username, this.password).subscribe({
      next: (response) => {
        localStorage.setItem('token', response.key);
        this.router.navigateByUrl(this.returnUrl);
        this.cleanForm();
      },
      error: (error) => {
        if (error.status === 400) {
          this.cleanForm();
        } else {
          console.log(error);
          this.cleanForm();
        }
      },
      complete: () => {
        console.log('response');
      }
    });
  }
}
