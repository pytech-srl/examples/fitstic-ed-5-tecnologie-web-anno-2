import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {

    constructor(private router: Router) {}

    canActivate(
	route: ActivatedRouteSnapshot,
	state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
	    const token = localStorage.getItem('token');
	    let auth = false;
	    if (token != null) {
		auth = true;
	    } else {
		this.router.navigate(['login'], {queryParams: {returnUrl: state.url}});
	    }
	    return auth;
	}
}
