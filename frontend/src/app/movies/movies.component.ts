import { Component, OnInit } from '@angular/core';

import { AuthService } from '../auth/auth.service';
import { MoviesService } from "./movies.service";
import { Movie } from "../models/Movie";

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
    movies!: Movie[];
    showForm: boolean = false;

    constructor(private moviesService: MoviesService) { }

    ngOnInit(): void {
	this.showMovies();
    }

    addMovie(movie: any): void {
	this.moviesService.addMovie(movie).subscribe(
	    () => {this.showMovies()}
	);
    }

    deleteMovie(movie: Movie): void {
	this.moviesService.deleteMovie(movie.id).subscribe(
	    () => {this.showMovies()}
	);
    }

    showMovies(): void {
	this.moviesService.getMovies()
	.subscribe(resp => {
	    this.movies = resp;
	});
    }

    toggleForm(): void {
	this.showForm = !this.showForm;
    }

}
