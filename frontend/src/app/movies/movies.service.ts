import { Injectable } from '@angular/core';
import  { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';

import {Movie} from "../models/Movie";

const httpOptions = {
    headers: new HttpHeaders({
	'Content-Type': 'application/json'
    })
}

@Injectable({
    providedIn: 'root'
})
export class MoviesService {
    moviesUrl = 'http://127.0.0.1:8000/api/v1/movies/movie';

    constructor(private http: HttpClient) {}

    addMovie(movie: any): Observable<any> {
	return this.http.post<any>(`${this.moviesUrl}/`, movie, httpOptions);
    }

    deleteMovie(id: number): Observable<Movie> {
	return this.http.delete<Movie>(`${this.moviesUrl}/${id}`, httpOptions);
    }

    getMovies(): Observable<Movie[]> {
	return this.http.get<Movie[]>(this.moviesUrl, httpOptions);
    }
}
