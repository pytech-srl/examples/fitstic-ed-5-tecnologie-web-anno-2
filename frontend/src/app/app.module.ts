import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from '@angular/core';

import { AddMovieComponent } from './add-movie/add-movie.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthInterceptor } from './auth/auth.interceptor';
import { AuthLoginComponent } from './auth/auth-login/auth-login.component';
import { AuthLogoutComponent } from './auth/auth-logout/auth-logout.component';
import { FormsModule} from '@angular/forms';
import { MovieItemComponent } from './movie-item/movie-item.component';
import { MoviesComponent } from './movies/movies.component';

@NgModule({
  declarations: [
    AddMovieComponent,
    AppComponent,
    AuthLoginComponent,
    AuthLogoutComponent,
    MovieItemComponent,
    MoviesComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
