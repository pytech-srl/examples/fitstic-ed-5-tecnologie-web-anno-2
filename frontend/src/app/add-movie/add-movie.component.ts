import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-add-movie',
  templateUrl: './add-movie.component.html',
  styleUrls: ['./add-movie.component.scss']
})
export class AddMovieComponent {

    title!: string;
    description!: string;
    @Output() addMovieEvent: EventEmitter<any> = new EventEmitter();

    submitAddMovieForm(): void {
	const movie = {
	    title: this.title,
	    description: this.description,
	}
	this.addMovieEvent.emit(movie);
	this.title = '';
	this.description = '';
    }

}
